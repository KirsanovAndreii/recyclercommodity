package com.ankir33gmail.recyclercommodity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by AnKir on 13.05.2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    Context context;
    List<User> users;

    public RecyclerAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);

        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);
        holder.textViewCommodity.setText(user.nameCommodity);
        holder.textViewamount.setText(user.amount);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView textViewCommodity;
        final TextView textViewamount;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewCommodity = (TextView) itemView.findViewById(R.id.text_view_commodity);
            textViewamount = (TextView) itemView.findViewById(R.id.text_view_amount);
        }
    }
}
